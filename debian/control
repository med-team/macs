Source: macs
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Michael R. Crusoe <crusoe@debian.org>,
           Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               pybuild-plugin-pyproject,
               python3-all-dev,
               python3-numpy,
               python3-setuptools,
               python3-pip,
               python3-pytest,
               python3-cykhash,
               python3-hmmlearn,
               libsimde-dev,
               cython3,
               help2man,
               procps,
               bc
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/macs
Vcs-Git: https://salsa.debian.org/med-team/macs.git
Homepage: https://github.com/taoliu/MACS/
Rules-Requires-Root: no

Package: macs
Architecture: any
Depends: ${python3:Depends},
         ${shlibs:Depends},
         ${misc:Depends},
         python3-hmmlearn
Description: Model-based Analysis of ChIP-Seq on short reads sequencers
 MACS empirically models the length of the sequenced ChIP fragments, which
 tends to be shorter than sonication or library construction size estimates,
 and uses it to improve the spatial resolution of predicted binding sites.
 MACS also uses a dynamic Poisson distribution to effectively capture local
 biases in the genome sequence, allowing for more sensitive and robust
 prediction. MACS compares favorably to existing ChIP-Seq peak-finding
 algorithms, is publicly available open source, and can be used for ChIP-Seq
 with or without control samples.
